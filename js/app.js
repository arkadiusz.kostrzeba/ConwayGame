function $(selector, container) {
    return (container || document).querySelector(selector);
}

(function(){
    
    
    var controls = {
        next: $('button.next'),
        auto: $('#autoplay'),
        interval: $('#interval')
    };
    
    var view = new View($('#grid'), 40, controls);
    console.log(view.style.rules);
    
    controls.next.addEventListener('click', function(){
        view.next();
        
    });
    
    controls.auto.addEventListener('change', function(evt){
        controls.next.disabled = this.checked;
        view.next();
    });
    
    controls.interval.addEventListener('change', function(evt){
        view.cellAnimationSpeed = this.value;
    });
})();