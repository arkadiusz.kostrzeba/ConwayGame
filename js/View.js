(function(){

var _ = self.View = function(table, size, controls){
    this.grid = table;
    this.size = size;
    this.controls = controls;
    
    this.createGrid();
    this.style = new DynamicStyle();
    this.createStyleRules();
};

_.prototype = {
    createGrid: function () {        
        var fragment = document.createDocumentFragment();
        this.grid.innerHTML = '';
        this.checkboxes = [];
        
        for (var y=0; y<this.size; y++) {
            var row = document.createElement('tr');
            this.checkboxes[y] = [];
            
            for (var x=0; x<this.size; x++) {
                var cell = document.createElement('td');
                var checkbox = document.createElement('input');
                checkbox.type = 'checkbox';
                checkbox.coords = [y, x];
                this.checkboxes[y][x] = checkbox;
                cell.appendChild(checkbox);
                row.appendChild(cell);       
            }
            fragment.appendChild(row);
        }
        
        this.grid.addEventListener('change', (function (evt){
            if(evt.target.nodeName == 'INPUT'){
                this.started = false;
            }
        }).bind(this));
        
        this.grid.addEventListener('keyup', (function (evt){
            var checkbox = evt.target;
            if(checkbox.nodeName === 'INPUT'){
                
                var coords = checkbox.coords;
                var y = coords[0];
                var x = coords[1];
                switch (evt.keyCode) {
                    case 37: //left
                        if(x > 0){
                            this.checkboxes[y][x-1].focus();
                        }
                        break;
                    case 38: //up
                        if(y > 0){
                            this.checkboxes[y-1][x].focus();
                        }
                        break;
                    case 39: //right
                        if(x + 1 < this.size){
                            this.checkboxes[y][x+1].focus();
                        }
                        break;
                    case 40: //bottom
                        if(y + 1 < this.size){
                            this.checkboxes[y+1][x].focus();
                        }
                        break;                        
                }
            }
        }).bind(this));
        
        this.grid.appendChild(fragment);
    },
    
    get boardArray() {
        return this.checkboxes.map(function (row) {
            return row.map(function (checkbox) {
                return +checkbox.checked;
            })    
        });
    },
    
    createStyleRules: function() {
        var animations = this.animationSettings;
        for(var i=0; i<animations.length; i++){
            var rule = animations[i].target + " { animation: "+ animations[i].name +" "+ animations[i].duration +"ms; }"
            this.style.insertRule(rule, i);
        }
    },
    
    get animationSettings() {
        return [
            // CELL ANIMATIONS
            {
                name: "birth",
                duration: this.controls.interval.value / 2,
                target: "#grid input[type=\"checkbox\"]:checked"
            }, 
            {   
                name: "death",
                duration: this.controls.interval.value / 8,
                target: "#grid input[type=\"checkbox\"]"
            }
        ];
    },
    
    set cellAnimationSpeed(value) {
        var animations = this.animationSettings;
        for(var i=0; i<animations.length;  i++) {
                    this.style.cssRules[i]
            .style.animation = [animations[i].name, animations[i].duration +"ms"].join(' ');
        }
    },
    
    load: function () {
        this.game = new Model(this.boardArray);
        this.loaded = true;
    },
    
    next: function () {
        if(!this.loaded || this.game){
            this.load();
        }
        
        this.game.next();
        var board = this.game.board;
        
        for( var y=0; y<this.size; y++){
            for( var x=0; x<this.size; x++){
                this.checkboxes[y][x].checked = !!board[y][x];
            }
        }
        
        if(this.controls.auto.checked){
            this.timer = setTimeout((function(){
                this.next();
            }).bind(this), this.controls.interval.value);
        } else {
            clearTimeout(this.timer)
        }
    }
};
})();